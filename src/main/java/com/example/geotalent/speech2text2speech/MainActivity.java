package com.example.geotalent.speech2text2speech;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private EditText edtResult;
    private int langResult;
    private String txtWord;
    private TextToSpeech toSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtResult = (EditText) findViewById(R.id.edtWord);
        toSpeech = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            Locale loc = new Locale("th");

            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    langResult = toSpeech.setLanguage(loc);
                } else {
                    Toast.makeText(getApplicationContext(), "Text to Speech not available", Toast.LENGTH_SHORT).show();
                }
            }
        }, "com.vajatts.nok");
    }

    public void onButtonClick(View v){
        switch(v.getId()){
            case R.id.imbMicInitTH:
                promptSpeechInput("th-TH");
                break;
            case R.id.imbMicInitEN:
                promptSpeechInput("en-US");
                break;
        }
    }

    private void promptSpeechInput(String language){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say something!");

        try{
            startActivityForResult(i, 100);
        }catch(ActivityNotFoundException a){
            Toast.makeText(MainActivity.this, "Sorry! Your device doesn't support speech language!", Toast.LENGTH_LONG).show();
        }
    }
    public void TTS(View view) {
        switch (view.getId()) {
            case R.id.imbPlay:
                txtWord = edtResult.getText().toString();
                if(txtWord.matches("")){
                    Toast.makeText(getApplicationContext(), "Please input your text.",Toast.LENGTH_SHORT).show();
                }else{
                    if (langResult == TextToSpeech.LANG_MISSING_DATA || langResult == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getApplicationContext(), "Language not available.", Toast.LENGTH_SHORT).show();
                    } else {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                            toSpeech.speak(txtWord, TextToSpeech.QUEUE_FLUSH, null,null);
                        }else{
                            toSpeech.speak(txtWord, TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                }

                break;
            case R.id.imbStop:
                if (toSpeech != null) {
                    toSpeech.stop();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        switch(requestCode){
            case 100: if(resultCode == RESULT_OK && i != null){
                ArrayList<String> result = i.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edtResult.setText(result.get(0));
            }
            break;
        }
    }
}
